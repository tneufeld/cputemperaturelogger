from time import sleep, strftime, time
from gpiozero import CPUTemperature
import matplotlib.pyplot as plt

cpu = CPUTemperature() #Gets the temperature

plt.ion() #Tells MatPlotLib this is a interactive plot
x = [] #Temperature
y = [] #Date

def write_temp(temp):
    with open("/home/pi/LOGS/CPUTemp.log", "a") as log:
        log.write("{},{}\n".format(strftime("%d/%m/%y,%H:%M:%S"), str(temp)))

def graph(temp):
    y.append(temp)
    x.append(time())
    plt.clf()
    plt.scatter(x,y)
    plt.plot(x,y)
    plt.draw()

while True:
    temp = cpu.temperature
    write_temp(temp)
#    graph(temp)
        
    sleep(1)
